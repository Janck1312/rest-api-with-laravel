<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        // \App\Models\User::factory(10)->create();

        // \App\Models\User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        // ]);

        \App\Models\Rol::create([
            'name'=>'superadmin',
            'description'=> 'user with all system\'s permissions',
            'code' => 'SUPERADMIN',
            'permissions' => json_encode(['prueba'])
        ]);

        \App\Models\User::create([
            'name' => 'Jancker',
            'email' => 'sepulved.janck@gmail.com',
            'email_verified_at' => \date('Y-m-d h:i:s'),
            'password' => Hash::make('12345678'),
            'rol_id' => '1'/*superadmin*/,
            'remember_token' => \date('Y-m-d h:i:s'),
        ]);
    }
}
