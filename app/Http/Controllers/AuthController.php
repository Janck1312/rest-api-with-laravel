<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\PostUser;
use App\Http\Requests\Auth;
use Illuminate\Support\Facades\Mail;

class AuthController extends Controller
{
    public function __construct()
    {  
        $this->middleware('auth:api', ['except' => ['login', 'signing']]);        
    }
    
    public function login ( Auth $auth ):array {
        $data = $auth->all();

        $user = User::where([ 'email' => $data['email'] ])->first();
        
        if($user) {

            if( Hash::check($auth->password, $user->password) && !(!$user->email_verified_at) ) {
                $token = auth()->attempt(['email' => $data['email'], 'password' => $data['password']]);
                
                return [
                    'success' => true,
                    'message' => 'user logued succesfully',
                    'token' => $token
                ];
            } else if($user->email_verified_at) {
                return [
                    'success' => false,
                    'message' => 'password don\'t match'
                ];
            }else if( !$user->email_verified_at && Hash::check($auth->password, $user->password) ) {
                return [
                    'success' => false,
                    'message' => 'should verify your email to login'
                ];
            }

        }else {
            return [
                'success' => false,
                'message' => 'user doesn\'t found on our database'
            ];
        }
    }

    public function logout():void {
        Auth::logout();
    }

    public function signin( PostUser $request ):array {
        $user = new User();
        $user->fill($request->all());
        $user->password = Hash::make( $request->password );
        $user->save();
        return [
            'success' => true,
            'message' => 'registered successfully'
        ];
    }

    public function sendEmailVerifyEmail( $user ) {
    }

    public function me() {
        return Auth()->user();
    }
}
