<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class Auth extends FormRequest
{   
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        return [
            'email' => ['required','email', 'string'],
            'password' => ['required', 'string']
        ];
    }

    public function messages():array {
        return [
            'email.required' => 'should send an email',
            'email.email' => 'should send a valid email',
            'password.required' => 'should send a password'
        ];
    }
}
